const tabs = document.querySelector('.tabs');
const tabsTitle = document.querySelectorAll('.tabs-title');
const arrayTabsTitle = Array.from(tabsTitle);

for (let i = 0; i < arrayTabsTitle.length; i++) {
    arrayTabsTitle[i].setAttribute('data-id', `tab${i + 1}`);
}

const tabsText = document.querySelectorAll('.tabs-content li');
const arrayTabsContent = Array.from(tabsText);

for (let i = 0; i < arrayTabsContent.length; i++) {
    arrayTabsContent[i].setAttribute('data-id', `tab${i + 1}`);
}

tabs.addEventListener('click', (ev) => {
    if (ev.target.dataset.id) {
        const clickedTabId = ev.target.dataset.id;
        arrayTabsTitle.forEach((tabTitle) => {
            return tabTitle.classList.remove('active');
        });
        const activeTabTitle = arrayTabsTitle.find((tabTitle) => {
            return tabTitle.dataset.id === clickedTabId;
        });
        activeTabTitle.classList.add('active');

        arrayTabsContent.forEach((tabContent) => {
            return tabContent.style.display = 'none';
        });

        const showTabContent = arrayTabsContent.find((tabContent) => {
            return tabContent.dataset.id === clickedTabId;
        });
        showTabContent.style.display = 'block';
    }
});